package test.cases.JiraTests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import pages.jira.JiraCreatePage;
import pages.jira.JiraLoginPage;

public class JiraTests extends BaseTest{

    public static final String EMAIL = "Put your email";
    public static final String PASSWORD = "Put your password";
    public static final String URL_AFTER_LOGIN = "https://start.atlassian.com/";
    public static final String PROJECT_NAME = "Put your project name";
    public static final String BUG_SUMMARY = "Website cannot be translated to German.";
    public static final String STORY_DESCRIPTION = String.format(
            "Title: Website translation functionality%nDescription:%nAs a user of phptravels.net%nI want to be able to " +
                    "see the website translated to my language%nSo that I can understand it.%nSteps to reproduce:%n1." +
                    "Navigate to [www.phptravels.net/en]%n2.Click on 'Language' in the right corner of the top of the " +
                    "page%n3.Choose 'German' from the drop down menu");

    public static final String Story_SUMMARY = "User should be able to translate the website to his native language";
    public static final String BUG_DESCRIPTION = String.format(
            "Title: Impossible to translate the main page to German%nSteps to reproduce:%n" +
                    "1.Navigate to www.phptravels.net/en%n2.Click on 'English' in the right corner of the top of the page%n" +
                    "3.Choose 'German' from the drop down menu%nExpected result: The whole page is translated in German.%n" +
                    "Actual result: The page is not translated and remains in English.");

    @Test
    public void login() {
        JiraLoginPage loginPage = new JiraLoginPage(actions.getDriver(), "jira.url");
        loginPage.authenticateUser(EMAIL, PASSWORD);
        loginPage.assertNavigated(URL_AFTER_LOGIN);
    }
    @Test
    public void createABug() {
        JiraLoginPage loginPage = new JiraLoginPage(actions.getDriver(), "jira.url");
        JiraCreatePage bugCreatePage = new JiraCreatePage(actions.getDriver(), "jira.loginURL");
        login();
       bugCreatePage.createABug(PROJECT_NAME, BUG_SUMMARY, BUG_DESCRIPTION);
       bugCreatePage.assertBugIsCreated();
        Assert.assertTrue("The element on the bug page wasn't found.",bugCreatePage.getTheLastLoadedElementOnTheBugPage().isDisplayed());
    }

    @Test
    public void createAStory () {
        JiraLoginPage loginPage = new JiraLoginPage(actions.getDriver(), "jira.url");
        JiraCreatePage bugCreatePage = new JiraCreatePage(actions.getDriver(), "jira.loginURL");
        createABug();
        bugCreatePage.createAStory(PROJECT_NAME, Story_SUMMARY, STORY_DESCRIPTION, BUG_SUMMARY);
    }

}
