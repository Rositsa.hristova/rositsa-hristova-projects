package pages.jira;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class JiraLoginPage extends BasePage {
    protected WebDriverWait wait;

    public JiraLoginPage(WebDriver driver, String urlKey) {
        super(driver, "jira.homePage");
        wait = new WebDriverWait(driver, Duration.ofSeconds(5));
    }
    public WebElement myAccountLogo() {
        return driver.findElement(By.id("imkt-jsx--3642d018"));
    }
    public WebElement findLogInButton() {return driver.findElement(By.xpath("//div[@class='sc-gPEVay cOfrRz']"));
    }
    public WebElement getEmail() {return driver.findElement(By.id("username"));
    }
    public WebElement getLoginButtonAtFirstLoginScreen() {return driver.findElement(By.id("login-submit"));
    }
    public WebElement getPassword() {return driver.findElement(By.id("password"));
    }
    public WebElement getLoginButtonAtSecondLoginScreen() {return driver.findElement(By.id("login-submit"));
    }

    public void authenticateUser (String username,String password) {
        wait.until(ExpectedConditions.elementToBeClickable(myAccountLogo()));
        myAccountLogo().click();
        findLogInButton().click();
        wait.until(ExpectedConditions.elementToBeClickable(getLoginButtonAtFirstLoginScreen()));
        getEmail().sendKeys(username);
        getLoginButtonAtFirstLoginScreen().submit();
        wait.until(ExpectedConditions.elementToBeClickable(getPassword()));
        getPassword().sendKeys(password);
        getLoginButtonAtSecondLoginScreen().submit();
    }
    public void assertNavigated (String expectedURL) {
        wait.until(ExpectedConditions.urlToBe(expectedURL));

    }

}
