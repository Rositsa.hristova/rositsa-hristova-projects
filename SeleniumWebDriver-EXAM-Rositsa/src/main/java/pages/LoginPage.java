package pages;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class LoginPage extends BaseHomePage {

    public LoginPage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void loginUser(String userKey) {
        String username = getConfigPropertyByKey("forum.users." + userKey + ".username");
        String password = getConfigPropertyByKey("forum.users." + userKey + ".password");

        navigateToPage();

        actions.waitForElementVisible("login.button");
        actions.clickElement("login.button");
        actions.waitForElementClickable("forum.email.field");
        actions.typeValueInField(username, "forum.email.field");
        actions.typeValueInField(password,"forum.password.field");
        actions.clickElement("forum.signIn.button");
    }
}
