package pages.jira;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;

public class JiraCreatePage extends JiraLoginPage{


    public JiraCreatePage(WebDriver driver, String urlKey) {
        super(driver, urlKey);
    }
    public WebElement swichToJiraSoftware() {
        return driver.findElement(By.xpath("//div[@class='sc-eHgmQL gMViXm']"));
    }
    public WebElement getProjectsButton() {
        return driver.findElement(By.xpath("//div[@class='css-17e7a4i']"));
    }
    public WebElement viewAllProjects() {
        return driver.findElement(By.linkText("View all projects"));
    }
    public WebElement chooseAProject(String projectName) {
        return driver.findElement(By.linkText(projectName));
    }
    public WebElement getCreateButton() {
       return driver.findElement(By.id("createGlobalItem"));
    }
    public  WebElement getIssueTypeDropDownMenu () {
        return driver.findElement(By.xpath("//div[@id='issue-create.ui.modal.create-form.type-picker.issue-type-select']"));
    }
    public  WebElement chooseBugAsAnIssueType () {
        return driver.findElement(By.xpath("//div[contains(text(), 'Bug')]"));
    }
    public  WebElement chooseStoryAsAnIssueType () {
        return driver.findElement(By.xpath("//div[contains(text(), 'Story')]"));
    }
    public WebElement getSummaryField () {
        return driver.findElement(By.id("summary-field"));
    }
    public WebElement getDescriptionField () {
        return  driver.findElement(By.xpath("//div[@role='textbox']"));
    }
    public WebElement findCreateButtonOnCreateWindow () {
        return driver.findElement(By.xpath("//button[@data-testid='issue-create.common.ui.footer.create-button']"));
    }
    public WebElement getTheLastLoadedElement () {
        return driver.findElement(By.id("board-tools-section-button"));
    }
    public WebElement getTheLastLoadedElementOnTheBugPage () {
        return driver.findElement(By.xpath("//button[@data-testid='issue-meatball-menu.ui.dropdown-trigger.button']"));
    }
    public WebElement getPriorityDropDown () {
         return driver.findElement(By.xpath("(//div[@class='sc-1lie33m-3 hpUCVg'])[last()]"));

    }
    public  WebElement setPriorityToHighest () {
        return driver.findElement(By.xpath("//div[contains(text(), 'Highest')]"));
    }
    public WebElement getLinkedIssueDropDown () {
        return driver.findElement(By.xpath("/html/body/div[13]/div[1]/div/div[3]/div/div/section/div[2]/div/div/div/div/form/div[6]/div/div/div/div[1]/div/div[1]/div[2]/div/span"));
    }
    public  WebElement getIsBlockedBy () {
        return driver.findElement(By.xpath("//div[contains(text(), 'is blocked by')]"));
    }
    public WebElement getIssuesList () {
        return driver.findElement(By.xpath("/html/body/div[13]/div[1]/div/div[3]/div/div/section/div[2]/div/div/div/div/form/div[6]/div/div/div/div[2]/div/div/div[1]/div[2]/div[2]/span"));

    }
    public WebElement chooseTheIssue(String bugSummary) {
        return driver.findElement(By.partialLinkText(bugSummary));

    }

    public void createABug(String projectName, String summary, String description) {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
        swichToJiraSoftware().click();
        getProjectsButton().click();
        viewAllProjects().click();
        chooseAProject(projectName).click();
        wait.until(ExpectedConditions.elementToBeClickable(getTheLastLoadedElement()));
        getCreateButton().click();
        wait.until(ExpectedConditions.elementToBeClickable(getIssueTypeDropDownMenu()));
        getIssueTypeDropDownMenu().click();
        chooseBugAsAnIssueType().click();
        getSummaryField().sendKeys(summary);
        getDescriptionField().sendKeys(description);
        getPriorityDropDown().click();
        setPriorityToHighest().click();

        findCreateButtonOnCreateWindow().click();
    }

    public void assertBugIsCreated() {
        driver.findElement(By.xpath("//a[@class='css-j800r4']")).click();
    }

    public void createAStory(String projectName, String summary, String description, String bugSummary) {

        wait.until(ExpectedConditions.elementToBeClickable(getTheLastLoadedElementOnTheBugPage()));
        getCreateButton().click();
        wait.until(ExpectedConditions.elementToBeClickable(getIssueTypeDropDownMenu()));
        getIssueTypeDropDownMenu().click();
        chooseStoryAsAnIssueType().click();
        getSummaryField().sendKeys(summary);
        getDescriptionField().sendKeys(description);
        getPriorityDropDown().click();
        setPriorityToHighest().click();
        getLinkedIssueDropDown().click();
        getIsBlockedBy().click();
        getIssuesList().click();
        chooseTheIssue(bugSummary).click();


        findCreateButtonOnCreateWindow().click();
    }
}
